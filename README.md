<img style="float: right" align="right" src="https://gitlab.com/pastel-fablab/orga/raw/master/logo/fablab-logo-transparent-300dpi.png" width="350">

# Démo Home Assistant
Ce projet est une démonstration d'installation domotique avec Home-Assistant adaptée à une maison de poupée.

4 scénarios sont identifés :
1. Gestion d'un carillion DIO issue du commerce (https://www.manomano.fr/carillon-et-sonnette-239)
2. Système d'alarme avec détection de présence et activation par badge RFID
3. Thermostat connecté.
4. Alumage automatique des lampes à la tombée de la nuit.

![architecture](./architecture.png)

## Fiche projet 
| **Caractéristiques du Projet**  |   |
| --- | --- |
| Technologies | Home-Assistant, ESP8266
| Coût | 100€
| Temps de réalisation | 10h
| Difficulté | Moyenne
| Equipe réalisatrice | Fabrice Teulé, Pierre Pochon

## Matériel

- maison de poupée (pour la démo)
- Box domotique Home-Assistant:
  - Raspberry Pi 3 ou +
  - Micro carte SD (16Go ou +)
  - Alimentation 
- 3 ESP8266
- 1 RFlink:
  - Arduino nano
  - Module récepteur 433 (RXB6)
- Capteurs / Actionneurs :
  - 3 leds de couleurs
  - 1 module RFID (RC522)
  - 1 capteur de température (DHT11)
  - 1 photorésistance
  - 1 détecteur de présence PIR (HC-SR501)
- Autres :
  - Câbles
  - 3 résistances de 10K
  - breadboards
  - alims 5V
  - Câbles microUSB

## Description détaillée 

Ce projet est une démonstration d'installation domotique avec Home-Assistant adaptée à une maison de poupée.

4 scénarios sont identifés :
1. Gestion d'un carillion DIO issue du commerce (https://www.manomano.fr/carillon-et-sonnette-239)
2. Système d'alarme avec détection de présence et activation par badge RFID
3. Thermostat connecté.
4. Alumage automatique des lampes à la tombée de la nuit.

### Installation Home-Assistant
[Voir espace dédié](hassio/README.md)

### Rflink
Voir tuto https://opendomotech.com/fabriquer-rflink-alternative-diy-du-rfxcom/

La partie émission n'est pas nécéssaire.

### ESP RFID
[Voir espace dédié](./esp_rfid)

### ESP 1
Cet ESP est flashé avec le framework ESPHome.
Le montage actuel permet lire/ecrire les éléments suivants :
- D1 : led jaune
- D2 : led rouge
- D3 : led rouge
- D4 : Capteur DHT11 (température et humidité)
- D5 : Buzzer.
- D6 : Capteur de détection infrarouge (PIR)
- A8 : Photorésistance (limunosité)

[Voir espace dédié](./esp_1)

## Liens

- Monter son Rflink : https://opendomotech.com/fabriquer-rflink-alternative-diy-du-rfxcom/
- Installation Home-assistant (hassio) : https://www.home-assistant.io/hassio/installation/
