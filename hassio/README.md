# Home Assistant

Cet espace concerne la mise en place de la box domotique Home Assistant, et de sa configuration associée.

## Installation

Voir https://www.home-assistant.io/hassio/installation/

## Configuration

### Add-ons

Les add-ons sont des fonctionnalités déployées sur le raspberry pi sous forme de conteneurs Docker.
Les add-on suivants seront installés :
- ssh server
- ESPHome 
- Mosquitto Broker

### Configuration add-on Mosquitto :
```
{
  "logins": [
    {
      "username": "TODO",
      "password": "TODO"
    }
  ],
  "anonymous": false,
  "customize": {
    "active": false,
    "folder": "mosquitto"
  },
  "require_certificate": false
}
```
