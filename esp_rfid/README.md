# ESP RFID

Cet ESP se connecte à un serveur MQTT pour envoyer le dernier identifiant de carte RFID détectée.
L'identifiant du dernier badge est envoyé dans le topic `home/rfid` du serveur MQTT (add-on Mosquitto sur le HASSIO)

## Configuration 
Il reste à définir les éléments suivants avant de flasher l'ESP :
- Wifi:
    - SSID
    - password
- MQTT:
    - IP
    - mqtt login 
    - mqtt password

# Cablage



Référence : https://community.home-assistant.io/t/a-simple-way-to-manually-control-alarm-system-with-rfid-reader/51430