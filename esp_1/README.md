# ESP 1

Cet ESP gère les capteurs/actionneurs suivants:  
- D1 : led jaune
- D2 : led rouge
- D3 : led rouge
- D4 : Capteur DHT11 (température et humidité)
- D5 : Buzzer.
- D6 : Capteur de détection infrarouge (PIR)
- A8 : Photorésistance (limunosité)


Il utilise le framwork **ESPHome** qui a été conçu pour l'interfacer très facilement avec Home-Assistant.
Le principe d'ESPHome est la définition simple des éléments cablés dans le fichier `esp_home_1.yaml`. Ce fichier doit être flashé via l'outil `esphome`. (cf https://esphome.io/guides/getting_started_command_line.html)

Un add-on hassio ESPHOME existe pour permettre l'intégration des capteurs et actionneurs dans Home-Assistant. Il suffira ensuite de déposer le fichier `esp_home_1.yaml` dans le dossier `esphome` de HASSIO.

## Câblage

![Cablage](./esp_1_bb.png)

## Références
- https://esphome.io/
- https://averagemaker.com/2018/04/wemos-led-control.html
- https://www.hackster.io/fattahalf/iot-connecting-and-interfacing-a-dht-11-with-wemos-d1-mini-feb843
- http://www.esp8266learning.com/wemos-mini-pir-sensor-example.php





